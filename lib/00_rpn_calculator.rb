# require 'byebug'
class RPNCalculator
  attr_accessor :calculator

  def initialize
    @stack = []
    @length = 0
  end

  def push(num)
    @stack.push(num)
    @length += 1
  end

  def plus
    self.checker
    popped = @stack.pop
    @stack[-1] += popped
    @length -= 1
  end

  def minus
    self.checker
    popped = @stack.pop
    @stack[-1] -= popped
    @length -= 1
  end

  def times
    self.checker
    popped = @stack.pop
    @stack[-1] *= popped
    @length -= 1
  end

  def divide
    self.checker
    popped = @stack.pop
    @stack[-1] = @stack[-1].fdiv(popped)
    @length -= 1
  end

  def value
    @stack[-1]
  end

  def tokens(tokens)
    operations = ["+","-","*","/"]
    tokens.split.map do |token|
      if operations.include?(token)
        token.to_sym
      else
        token.to_f
      end
    end
  end

  def evaluate(str)
    # debugger
    toks = self.tokens(str)
    i = 0
    while i < toks.length
      if toks[i].is_a?(Float)
        self.push(toks[i])
      elsif toks[i] == :+
        self.plus
      elsif toks[i] == :-
        self.minus
      elsif toks[i] == :*
        self.times
      elsif toks[i] == :/
        self.divide
      end

      i += 1
    end
    self.value

    # self.tokens(str).each do |item|
    #   if item.is_a?(Float)
    #     @stack.push(item)
    #   elsif item == :+
    #     self.plus
    #   elsif item == :-
    #     self.minus
    #   elsif item == :*
    #     self.times
    #   elsif item == :/
    #     self.divide
    #   end
    # end
    # self.value
  end

  def checker
    if @length < 2
      raise "calculator is empty"
    end
  end
end
